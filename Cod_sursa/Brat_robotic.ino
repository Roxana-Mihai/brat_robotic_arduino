/************************************** BRAT ROBOTIC IMPRIMAT 3D **************************************
 *  
 *  Autori: 
 *    Roxana-Florentina MIHAI - 332AA
 *    
 */

/******** BIBLIOTECILE FOLOSITE ********/

#include <SoftwareSerial.h>
#include <Servo.h>

/******** INITIALIZARI ********/

Servo servo01;    // MOTOR SERVO MG996 - baza
Servo servo02;    // MOTOR SERVO MG996 - umar
Servo servo03;    // MOTOR SERVO MG996 - cot
Servo servo04;    // MOTOR SERVO SG90 9G - rotire incheietura
Servo servo05;    // MOTOR SERVO SG90 9G - inclinare incheietura
Servo servo06;    // MOTOR SERVO SG90 9G - cleste

SoftwareSerial Bluetooth(3, 4);

int servo1Pos, servo2Pos, servo3Pos, servo4Pos, servo5Pos, servo6Pos;         // Variabilele in care stocam pozitia curenta a servomotoarelor
int servo1PPos, servo2PPos, servo3PPos, servo4PPos, servo5PPos, servo6PPos;   // Variabile in care stocam pozitia anterioara a servomotoarelor

String dataIn = "";    // Variabila in care stocam datele citite de la aplicatia mobila

/******** SETUP ********/

void setup() {

  /* Se conecteaza fiecare servomotor la pinii corespunzatori de pe placuta Arduino */
  servo01.attach(5);
  servo02.attach(6);
  servo03.attach(7);
  servo04.attach(8);
  servo05.attach(9);
  servo06.attach(10);
  
  Bluetooth.begin(9600);    // Baud rate-ul la care functioneaza modulul bluetooth HC-05
  Bluetooth.setTimeout(5);
  delay(20);

  /* Setarea pozitiilor initale pentru fiecare motor, in functie de specificatiile acestuia */
    /* 
     *  Pentru MOTOR SERVO MG996:
     *      0 - rotire cu viteza maxima spre stanga
     *     90 - stationare
     *    180 - rotire cu viteza maxima spre dreapta
     *  
     *  Pentru MOTOR SERVO SG90 9G:
     *      0 - rotire la 0 grade
     *     90 - rotire la 90 de grade
     *    180 - rotire la 180 grade
     */
     
  servo1PPos = 90;
  servo01.write(servo1PPos);
  servo2PPos = 90;
  servo02.write(servo2PPos);
  servo3PPos = 90;
  servo03.write(servo3PPos);
     
  servo4PPos = 0;
  servo04.write(servo4PPos);
  servo5PPos = 0;
  servo05.write(servo5PPos);
  servo6PPos = 0;
  servo06.write(servo6PPos);

}

/******** LOOP ********/

void loop() {
  
  /* Verificarea primirii datelor de la modulul bluetooth */
  if (Bluetooth.available() > 0) {
    dataIn = Bluetooth.readString();  // Se citesc datele transmise de la aplicatie si se memoreaza in dataIn
    delay(10);
   
    /*
     * In functie de datele primite, se decide ce motor trebuie actionat.
     * Cand pozitia unui slider este modificata, aplicatia de pe mobil transmite
     * catre Arduino un string ce contine un cod pentru identificarea servomotorului,
     * urmat de noua valoare setata prin intermediul acestuia.
    */ 
    
    /* Daca pozitia slider-ului pentru BAZA robotului a fost modificata, se actioneaza primul servomotor - servo01. */
    if (dataIn.startsWith("s1")) {
      String dataInS = dataIn.substring(2, dataIn.length());  // Se memoreaza in dataInS doar partea care reprezinta pozitia dorita a servomotorului
      servo1Pos = dataInS.toInt();                            // Convertirea acesteia din string in integer
      
      /* 
       * Deoarece motorul se invarte continuu (vezi Documentatie - probleme intampinate), se pastreaza pozitia precedenta la 90 (motor oprit)
       * 
       * Daca valoarea transmisa de la aplicatie este MAI MARE DE 90, atunci se deplaseaza motorul pentru o durata de 650 ms,
       * pana cand acesta este oprit. Miscarea determinata este echivalenta cu o deplasare de aproximativ 45 de grade.
       */
      if (servo1PPos > servo1Pos) {
        servo01.write(85);
        delay(650);
        servo01.write(90);
      }
      /* 
       * Daca valoarea transmisa de la aplicatie este MAI MICA DE 90, atunci se deplaseaza motorul pentru o durata de 550 ms in sensul opus,
       * pana cand acesta este oprit. Miscarea determinata este echivalenta cu o deplasare de aproximativ 45 de grade.
       */
      if (servo1PPos < servo1Pos) {
        servo01.write(100);
        delay(550);
        servo01.write(90);
      }
    }
    
    /* Daca pozitia slider-ului pentru UMARUL robotului a fost modificata, se actioneaza al doilea servomotor - servo02. */
    if (dataIn.startsWith("s2")) {
      String dataInS = dataIn.substring(2, dataIn.length());   // Se memoreaza in dataInS doar partea care reprezinta pozitia dorita a servomotorului
      servo2Pos = dataInS.toInt();                             // Convertirea acesteia din string in integer

      /* 
       * Deoarece motorul se invarte continuu (vezi Documentatie - probleme intampinate), se pastreaza pozitia precedenta la 90 (motor oprit)
       * 
       * Daca valoarea transmisa de la aplicatie este MAI MARE DE 90, atunci se deplaseaza motorul pentru o durata de 650 ms,
       * pana cand acesta este oprit. Miscarea determinata este echivalenta cu o deplasare de aproximativ 45 de grade.
       */
      if (servo2PPos > servo2Pos) {
        servo02.write(85);
        delay(650);
        servo02.write(90);
      }
      /* 
       * Daca valoarea transmisa de la aplicatie este MAI MICA DE 90, atunci se deplaseaza motorul pentru o durata de 550 ms in sensul opus,
       * pana cand acesta este oprit. Miscarea determinata este echivalenta cu o deplasare de aproximativ 45 de grade.
       */
      if (servo2PPos < servo2Pos) {
        servo02.write(100);
        delay(550);
        servo02.write(90);
      }
    }
    
    /* Daca pozitia slider-ului pentru COTUL robotului a fost modificata, se actioneaza al treilea servomotor - servo03. */
    if (dataIn.startsWith("s3")) {
      String dataInS = dataIn.substring(2, dataIn.length());   // Se memoreaza in dataInS doar partea care reprezinta pozitia dorita a servomotorului
      servo3Pos = dataInS.toInt();                             // Convertirea acesteia din string in integer

      /* 
       * Deoarece motorul se invarte continuu (vezi Documentatie - probleme intampinate), se pastreaza pozitia precedenta la 90 (motor oprit)
       * 
       * Daca valoarea transmisa de la aplicatie este MAI MARE DE 90, atunci se deplaseaza motorul pentru o durata de 650 ms,
       * pana cand acesta este oprit. Miscarea determinata este echivalenta cu o deplasare de aproximativ 45 de grade.
       */
      if (servo3PPos > servo3Pos) {
        servo03.write(85);
        delay(650);
        servo03.write(90);
      }
      /* 
       * Daca valoarea transmisa de la aplicatie este MAI MICA DE 90, atunci se deplaseaza motorul pentru o durata de 550 ms in sensul opus,
       * pana cand acesta este oprit. Miscarea determinata este echivalenta cu o deplasare de aproximativ 45 de grade.
       */
      if (servo3PPos < servo3Pos) {
        servo03.write(100);
        delay(550);
        servo03.write(90);
      }
    }
    
    /* 
     * Daca pozitia slider-ului pentru MISCAREA DE ROTATIE A INCHEIETURII robotului a fost modificata, 
     * se actioneaza al patrulea servomotor - servo04. 
     */
    if (dataIn.startsWith("s4")) {
      String dataInS = dataIn.substring(2, dataIn.length());   // Se memoreaza in dataInS doar partea care reprezinta pozitia dorita a servomotorului
      servo4Pos = dataInS.toInt();                             // Convertirea acesteia din string in integer

      /* 
       *  Pentru a preveni o miscare la viteza maxima a servomotorului, ce ar putea duce la defectarea machetei,
       *  se foloseste un for (care porneste de la pozitia precedenta si merge pana la pozitia curenta) pentru a
       *  efectua aceasta deplasare treptat. (viteza deplasarii depinde de durata delay-ului)
       */
      if (servo4PPos > servo4Pos) {
        for ( int j = servo4PPos; j >= servo4Pos; j--) {
          servo04.write(j);
          delay(30);
        }
      }
      if (servo4PPos < servo4Pos) {
        for ( int j = servo4PPos; j <= servo4Pos; j++) {
          servo04.write(j);
          delay(30);
        }
      }
      /*  Actuala pozitie a servomotorului este salvata drept pozitie precedenta, 
       *  pentru a servi drept comparatie atunci cand se transmite o noua pozitie
       */
      servo4PPos = servo4Pos;   
    }
    
    /* 
     * Daca pozitia slider-ului pentru MISCAREA DE INCLINARE A INCHEIETURII robotului a fost modificata, 
     * se actioneaza al cincilea servomotor - servo05. 
     */
    if (dataIn.startsWith("s5")) {
      String dataInS = dataIn.substring(2, dataIn.length());   // Se memoreaza in dataInS doar partea care reprezinta pozitia dorita a servomotorului 
      servo5Pos = dataInS.toInt();                             // Convertirea acesteia din string in integer

      /* 
       *  Pentru a preveni o miscare la viteza maxima a servomotorului, ce ar putea duce la defectarea machetei,
       *  se foloseste un for (care porneste de la pozitia precedenta si merge pana la pozitia curenta) pentru a
       *  efectua aceasta deplasare treptat. (viteza deplasarii depinde de durata delay-ului)
       */
      if (servo5PPos > servo5Pos) {
        for ( int j = servo5PPos; j >= servo5Pos; j--) {
          servo05.write(j);
          delay(30);
        }
      }
      if (servo5PPos < servo5Pos) {
        for ( int j = servo5PPos; j <= servo5Pos; j++) {
          servo05.write(j);
          delay(30);
        }
      }
      /*  Actuala pozitie a servomotorului este salvata drept pozitie precedenta, 
       *  pentru a servi drept comparatie atunci cand se transmite o noua pozitie
       */
      servo5PPos = servo5Pos;
    }

    /* 
     * Daca pozitia slider-ului pentru MISCAREA CLESTELUI robotului a fost modificata, 
     * se actioneaza al saselea servomotor - servo06. 
     */
    if (dataIn.startsWith("s6")) {
      String dataInS = dataIn.substring(2, dataIn.length());   // Se memoreaza in dataInS doar partea care reprezinta pozitia dorita a servomotorului
      servo6Pos = dataInS.toInt();                             // Convertirea acesteia din string in integer

      /* 
       *  Pentru a preveni o miscare la viteza maxima a servomotorului, ce ar putea duce la defectarea machetei,
       *  se foloseste un for (care porneste de la pozitia precedenta si merge pana la pozitia curenta) pentru a
       *  efectua aceasta deplasare treptat. (viteza deplasarii depinde de durata delay-ului)
       */
      if (servo6PPos > servo6Pos) {
        for ( int j = servo6PPos; j >= servo6Pos; j--) {
          servo06.write(j);
          delay(30);
        }
      }
      if (servo6PPos < servo6Pos) {
        for ( int j = servo6PPos; j <= servo6Pos; j++) {
          servo06.write(j);
          delay(30);
        }
      }
      /*  Actuala pozitie a servomotorului este salvata drept pozitie precedenta, 
       *  pentru a servi drept comparatie atunci cand se transmite o noua pozitie
       */
      servo6PPos = servo6Pos;
    }
  }
}
